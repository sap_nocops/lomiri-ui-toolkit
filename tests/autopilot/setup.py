#!/usr/bin/python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
#
# lomiri-ui-toolkit Autopilot Test Suite
# Copyright (C) 2012, 2013, 2015 Canonical Ltd.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from setuptools import setup

setup(
    name='lomiriuitoolkit',
    version='1.3.5100',
    description='lomiri-ui-toolkit autopilot tests.',
    url='https://gitlab.com/ubports/development/core/lomiri-ui-tookit',
    license='LGPLv3',
    packages=[ 'lomiriuitoolkit' ],
)
